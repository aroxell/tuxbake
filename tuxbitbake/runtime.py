import os
import re
import shlex
import subprocess
import sys
import time
from functools import lru_cache
from pathlib import Path


DEFAULT_RUNTIME = "docker"
DEFAULT_CONTAINER_REGISTRY = "docker.io"


class Runtime:
    basedir = "runtime"
    name = "runtime"
    exception = Exception
    bindir = Path(__file__).parent / basedir / "bin"

    def get(name):
        name = name or DEFAULT_RUNTIME
        name = "".join([w.title() for w in re.split(r"[_-]", name)]) + "Runtime"
        try:
            here = sys.modules[__name__]
            cls = getattr(here, name)
            return cls()
        except AttributeError:
            raise InvalidRuntimeError(name)

    def __init__(self):
        super().__init__()

    def __init_config__(self):
        self.toolchains = Toolchain.supported()

    def get_image(self):
        return None

    def get_command_line(self, build, cmd, interactive):
        prefix = self.get_command_prefix(interactive)
        return [*prefix, *cmd]

    def get_command_prefix(self, interactive):
        return []

    def prepare(self, build):
        pass

    def cleanup(self):
        pass

    def get_metadata(self, build):
        return {}


class ContainerRuntime(Runtime):
    prepare_failed_msg = "failed to pull remote image {image}"
    bindir = Path("/tuxbitbake")

    def __init_config__(self):
        self.base_images = []
        self.ci_images = []
        self.container_id = None

    @lru_cache(None)
    def get_image(self, container):
        image = f"docker.io/vishalbhoj/{container}"
        return image

    def prepare(self, build):
        super().prepare(build)
        try:
            self.prepare_image(build)
            self.start_container(build)
        except subprocess.CalledProcessError:
            raise RuntimePreparationFailed(
                self.prepare_failed_msg.format(image=self.get_image(build.container))
            )

    def prepare_image(self, build):
        pull = [self.command, "pull", self.get_image(build.container)]
        now = time.time()
        subprocess.check_call(pull)

    def start_container(self, build):
        machine_env = distro_env = ""
        src_dir = os.path.abspath(build.src_dir)
        if build.dl_dir:
            dl_dir = os.path.abspath(build.dl_dir)

        bin_volume = self.volume(super().bindir, self.bindir)

        env = (f"--env={k}={v}" for k, v in build.environment.items())
        env_list = list(env)
        if build.machine:
            env_list.append(f"--env=MACHINE={build.machine}")
        if build.distro:
            env_list.append(f"--env=DISTRO={build.distro}")
        if build.dl_dir:
            env_list.append(f"--volume={build.dl_dir}:{build.dl_dir}")

        user_opts = self.get_user_opts()
        extra_opts = self.__get_extra_opts__()
        cmd = [
            self.command,
            "run",
            "--rm",
            "--init",
            "--detach",
            *env_list,
            *user_opts,
            self.volume(src_dir, src_dir),
            bin_volume,
            f"--workdir={src_dir}",
            *self.get_logging_opts(),
            *extra_opts,
            self.get_image(build.container),
            "sleep",
            "1d",
        ]
        self.container_id = self.spawn_container(cmd)

    def spawn_container(self, cmd):
        return subprocess.check_output(cmd).strip().decode("utf-8")

    def get_command_prefix(self, interactive):
        if interactive:
            interactive_opts = ["--interactive", "--tty"]
        else:
            interactive_opts = []
        return [self.command, "exec", *interactive_opts, self.container_id]

    def cleanup(self):
        if not self.container_id:
            return
        subprocess.check_call(
            [self.command, "stop", self.container_id], stdout=subprocess.DEVNULL
        )

    def __get_extra_opts__(self):
        opts = os.getenv(self.extra_opts_env_variable, "")
        return shlex.split(opts)

    def get_metadata(self, build):
        image_name = self.get_image()
        image_digest = subprocess.check_output(
            [
                self.command,
                "image",
                "inspect",
                "--format='{{index .RepoDigests 0}}",
                image_name,
            ],
        ).decode("utf-8")
        return {
            "image_name": image_name,
            "image_digest": image_digest,
        }


class DockerRuntime(ContainerRuntime):
    name = "docker"
    command = "docker"
    extra_opts_env_variable = "TUXBITBAKE_DOCKER_RUN"

    def get_user_opts(self):
        uid = os.getuid()
        gid = os.getgid()
        return [f"--user={uid}:{gid}"]

    def get_logging_opts(self):
        return []

    def volume(self, source, target):
        return f"--volume={source}:{target}"
