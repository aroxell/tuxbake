# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2021-present Linaro Limited
#
# SPDX-License-Identifier: MIT

"""
Command line tool to build OpenEmbedded and Yocto images
"""
__version__ = "0.0.1"
