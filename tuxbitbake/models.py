# -*- coding: utf-8 -*-

from dataclasses import asdict, dataclass, field, fields
from typing import Dict, List
from tuxbitbake.runtime import Runtime

import json
import subprocess
import os
import sys
import shlex


class Base:
    def as_dict(self):
        return asdict(self)

    def as_json(self):
        return json.dumps(self.as_dict())

    @classmethod
    def new(cls, **kwargs):
        fields_names = [f.name for f in fields(cls)]
        i_kwargs = {}
        v_kwargs = {}
        for k in kwargs:
            if k in fields_names:
                v_kwargs[k] = kwargs[k]
            else:
                i_kwargs[k] = kwargs[k]

        return cls(**v_kwargs, extra=i_kwargs)


@dataclass
class OEBuild(Base):
    src_dir: str
    build_dir: str
    envsetup: str
    target: str
    artifacts: List[str]
    repo_manifest: str = "default.xml"
    distro: str = None
    machine: str = None
    container: str = None
    environment: Dict = None
    local_conf: List[str] = None
    bblayers_conf: List[str] = None
    sstate_mirror: str = None
    dl_dir: str = None
    extra: Dict = None
    __logger__ = None

    def validate(self):
        return

    def prepare(self):
        self.runtime = Runtime.get("docker")
        self.runtime.prepare(self)
        with open(
            f"{os.path.abspath(self.src_dir)}/extra_local.conf", "w"
        ) as extra_local_conf:
            if self.dl_dir:
                extra_local_conf.write(f'DL_DIR = "{self.dl_dir}"\n')
            if self.sstate_mirror:
                extra_local_conf.write(f'SSTATE_MIRRORS = "{self.sstate_mirror}"\n')
                extra_local_conf.write('USER_CLASSES += "buildstats buildstats-summary"\n')
            if self.local_conf:
                for line in self.local_conf:
                    extra_local_conf.write(f'{line}\n')

        if self.bblayers_conf:
            with open(
                f"{os.path.abspath(self.src_dir)}/bblayers.conf", "w"
                ) as bblayers_conf_file:
                for line in self.bblayers_conf:
                    bblayers_conf_file.write(f'{line}\n')
        return

    def run_cmd(self, cmd):
        logger = self.logger.stdin
        stdout = stderr = logger
        stdin = subprocess.DEVNULL
        self.log(" ".join([shlex.quote(c) for c in cmd]))
        process = subprocess.Popen(
            cmd,
            cwd=self.src_dir,
            env=self.environment,
            stdin=stdin,
            stdout=stdout,
            stderr=stderr,
        )
        process.communicate()
        return process.returncode == 0

    def do_build(self):
        cmd = self.runtime.get_command_line(
            self,
            [
                "bash",
                "-c",
                f"source {self.envsetup} {self.build_dir}; cat ../extra_local.conf >> conf/local.conf; cat ../bblayers.conf >> conf/bblayers.conf || true; echo 'Dumping local.conf..'; cat conf/local.conf; bitbake -e > bitbake-environment; bitbake {self.target}",
            ],
            interactive=False,
        )
        if self.run_cmd(cmd):
            self.result = "pass"
        else:
            self.result = "fail"

    def do_cleanup(self):
        self.runtime.cleanup()

    @property
    def logger(self):
        if not self.__logger__:
            stdout = sys.stdout
            self.__logger__ = subprocess.Popen(
                [
                    str(Runtime.bindir / "tuxbitbake-logger"),
                    str(f"{os.path.abspath(self.src_dir)}/build.log"),
                    str(f"{os.path.abspath(self.src_dir)}/build-debug.log"),
                ],
                stdin=subprocess.PIPE,
                stdout=stdout,
            )
        return self.__logger__

    def log(self, *stuff):
        subprocess.call(["echo"] + list(stuff), stdout=self.logger.stdin)
