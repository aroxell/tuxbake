#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2021-present Linaro Limited
#
# SPDX-License-Identifier: MIT

import argparse


##########
# Setups #
##########
def setup_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(prog="tuxbitbake", description="TuxBitBake")
    #    parser.add_argument(
    #    "--version", action="version", version=f"%(prog)s, {__version__}"
    # )
    group = parser.add_argument_group("OE Build Parameters")
    group.add_argument(
        "--build-definition",
        help="Specify json file with build parameters",
        default=None,
    )
    group.add_argument(
        "--src-dir", help="source directory containing all the layers", default=None
    )
    group.add_argument(
        "--build-dir",
        help="build directory where the build output is published",
        default=None,
    )
    return parser
