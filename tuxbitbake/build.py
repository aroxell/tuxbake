#!/usr/bin/python3
# -*- coding: utf-8 -*-
# vim: set ts=4
#
# Copyright 2021-present Linaro Limited
#
# SPDX-License-Identifier: MIT

from tuxbitbake.argparse import setup_parser
from tuxbitbake.models import OEBuild
import json


def build(**kwargs):
    oebuild = OEBuild(**kwargs)
    oebuild.prepare()
    oebuild.do_build()
    oebuild.do_cleanup()
    return oebuild
